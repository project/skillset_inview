# Skillset Inview Readme


## Introduction

  For your portfolio!

  Provides a block of your key skills with an animated bar graph.
  Animation is triggered when block is 'Inview' of user,
  resets when out of view.

  with Help Module active see: [/admin/help/skillset_inview](url)


##  Requirements

1.   jquery.inview.min.js (v1.1.2)  [https://github.com/protonet/jquery.inview/tree/v1.1.2](url)
Move into:  /libraries/jquery.inview/jquery.inview.min.js
1.  jquery.easing.min.js (v1.3.2)   [https://github.com/gdsmith/jquery.easing/tree/1.3.2 ](url)  Move into /libraries/jquery.easing/jquery.easing.min.js
1.  farbtastic(v2.0.0a1)   [https://github.com/mattfarina/farbtastic/tree/1.3u ](url)  Move into /libraries/farbtastic/farbtastic.min.js and all contents


##  Installation

  1. install into /modules.
  2. refer to Requirements (above) to install required Javascript plugins.
  3. enable module


##  Configuration

  [/admin/people/permissions#module-skillset_inview](url) adjust permission as needed
  [/admin/content/skillset-inview/add](url) to add content.
  [/admin/content/skillset-inview](url) to order content.
  [/admin/structure/block](url) to place the skillset inview block


## Troubleshooting

  make sure jQuery plugins are installed properly


 ## FAQ

  Comes with grids ready css classes (at this time you must include by
  your own means. assuming your theme will use) module provides .tpl
  for html & css class changes as your theming needs require


## Maintainers

  https://www.drupal.org/user/297907
  SKAUGHT
