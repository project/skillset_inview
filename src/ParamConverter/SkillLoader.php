<?php

namespace Drupal\skillset_inview\ParamConverter;

use Drupal\Core\ParamConverter\ParamConverterInterface;
use Symfony\Component\Routing\Route;

/**
 * Class SkillLoader.
 *
 * @package Drupal\skillset_inview\ParamConverter
 */
class SkillLoader implements ParamConverterInterface {

  /**
   * {@inheritdoc}
   */
  public function convert($id, $definition, $name, array $defaults) {
    // TODO: Drupal Rector Notice: Please delete the following comment after you've made any necessary changes.
    // You will need to use `\Drupal\core\Database\Database::getConnection()` if you do not yet have access to the container here.
    $item = \Drupal::database()->query("SELECT id, weight, name, percent FROM {skillset_inview} WHERE id = :id", [':id' => $id])->fetchObject();
    return $item;
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route) {
    return (!empty($definition['type']) && $definition['type'] == 'skillset_inview');
  }

}
